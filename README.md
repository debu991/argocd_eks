# How to install ArgoCD
```
kubectl create ns argocd || true
kubectl apply -n argocd -f https://raw.githubusercontent.com/argoproj/argo-cd/stable/manifests/install.yaml'
kubectl patch svc argocd-server -n argocd -p '{"spec": {"type": "LoadBalancer"}}'
```

# Kubernetes components
* aws-node-termination-handler

  Gracefully handle EC2 instance shutdown within Kubernetes

* cluster-autoscaler

  Cluster Autoscaler is a tool that automatically adjusts the size of the Kubernetes cluster when one of the following conditions is true:
    * there are pods that failed to run in the cluster due to insufficient resources.
    * there are nodes in the cluster that have been underutilized for an extended period of time and their pods can be placed on other existing nodes. 

* fluentbit

  Fluent Bit is a fast Log Processor and Forwarder for Linux

* hpa-operator

  Horizontal Pod Autoscaler operator watches for your Deployment or StatefulSet and automatically creates an HorizontalPodAutoscaler resource, allow you provide the correct autoscale annotations.

* kubernetes-external-secrets
  Kubernetes External Secrets allows you to use external secret management systems, like AWS Secrets Manager or HashiCorp Vault, to securely add secrets in Kubernetes.

* metrics-server

  Metrics Server collects resource metrics from Kubelets and exposes them in Kubernetes apiserver through Metrics API for use by Horizontal Pod Autoscaler and Vertical Pod Autoscaler. Metrics API can also be accessed by kubectl top, making it easier to debug autoscaling pipelines.

* prometheus

  Prometheus is a systems and service monitoring system. It collects metrics from configured targets at given intervals, evaluates rule expressions, displays the results, and can trigger alerts when specified conditions are observed.

* traefik

  Traefik is a modern HTTP reverse proxy and load balancer that makes deploying microservices easy. It receives requests on behalf of your system and finds out which components are responsible for handling them.

* velero

  Velero is an open source tool to safely backup and restore, perform disaster recovery, and migrate Kubernetes cluster resources and persistent volumes.
